****VPS - Vacation Planning System****

**Technologie a jazyk** : Java - Spring Boot, E-mail client, Docker

**Databáze** : PostgreSQL

**Cache** : Redis (v service třídách uvnitř cz.cvut.fel.nss.vps.service)

**Messaging** : Kafka (části spojené s implementaci Kafky se nachází v balíčkách kafka, např.: cz.cvut.fel.nss.vps.service.kafka)

**Zabezpečení** : JWT autentizace (cz.cvut.fel.nss.vps.security)

**Interceptors** : (cz.cvut.fel.nss.vps.config.interceptor)

**Využité REST API** (cz.cvut.fel.nss.vps.controller.rest)

**Nasazeno na Heroku** : https://nss-vps.herokuapp.com

**Architektura** : vícevrstevnatá

**Elastic search** : části spojené s implementaci ES se nachází v balíčkách es (např. cz.cvut.fel.nss.repository.es)

**Design patterny** :
			
			- Dependency Injection + Repository (DI napříč celého projektu, např: cz.cvut.fel.nss.vps.service.HolidayRequestService, repository: cz.cvut.fel.nss.vps.repository) 
			
			- Factory (cz.cvut.fel.nss.vps.patterns.factory),
			
			- Facade (cz.cvut.fel.nss.vps.patterns.facade), 
			
			- Visitor (cz.cvut.fel.nss.vps.patterns.visitor), 
			
			- Observer (cz.cvut.fel.nss.vps.patterns.observer),
			
			- Interceptor (cz.cvut.fel.nss.vps.config.interceptor)
			
**8 UC** : v projektové dokumentaci


**Postup při spouštění aplikace** : 

	Aplikace se dá spustit dvěma způsoby, přes cloudovou platformu Heroku nebo lokálně. Při spouštění přes Heroku je třeba mít připojení na internet a přes webový prohlížeč přejít na stránku https://nss-vps.herokuapp.com.

	Při spouštění lokálně je třeba mít stažený soubor vps.jar, který se nachází v souborech tohoto projektu  a přes command line spustit aplikaci příkazem: java -jar /vps.jar . Před spuštěním je také nutné mít stažené a nainstalované: PostgreSQL, Elastic search, Kafka, Zookeeper a Redis. Jeden ze způsobů jak to udělat je přes software Docker. V Dockeri jsou images pro stažení aplikaci následující:

		Kafka: wurstmeister / kafka: latest
		Zookeeper: wurstmeister / Zookeeper: latest
		Redis: redis: latest
		PostgreSQL: postgres: latest
		Elastic Search: docker.elastic.co/elasticsearch/elasticsearch:7.12.0 
	
	Data pro inicializaci jsou v cz.cvut.fel.nss.vps.DBInit. 

	Přihlašovací údaje pro admina: 
		- username : admin
		- password : password

	Přihlašovací údaje pro usera: 
		- username : user
		- password : heslo

	

**Funkcionalita (také v projektové dokumentaci) např** :

		Zobrazení žádosti o dovolenou od týmu a podle stavu žádosti
		Calendar-like view
		Rozesílání notifikačních emailů při schválení / zamítnutí dovolené	
		Zobrazení žádosti o dovolenou od týmu
		Přidání / odebrání zaměstnance z týmu 
		Schválení / zamítnutí žádosti o dovolenou 
		Zobrazení žádosti o dovolenou od uživatele
		Přidání / úprava / mazání uživatele, týmu, role, žádosti o dovolenou, státního svátku
		Přiřazení týmové role uživateli 

**Autoři** :
		
	- Oľga Ostashchuk
		
	- Michal Pechník
		
	- Martin Nešněra

	- Michal Šalaga
