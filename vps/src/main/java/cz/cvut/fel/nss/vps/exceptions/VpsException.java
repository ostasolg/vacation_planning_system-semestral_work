package cz.cvut.fel.nss.vps.exceptions;

/**
 * Base for all application-specific exceptions.
 */
public class VpsException extends RuntimeException {

    public VpsException() {
    }

    public VpsException(String message) {
        super(message);
    }

    public VpsException(String message, Throwable cause) {
        super(message, cause);
    }

    public VpsException(Throwable cause) {
        super(cause);
    }
}
