package cz.cvut.fel.nss.vps.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "teams",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "name")
        })
public class Team extends AbstractEntity {

    @NotBlank
    @Size(min = 1, max = 30, message = "Team name must be between 1 and 30 characters.")
    private String name;

    @ToString.Exclude
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(mappedBy = "team", fetch = FetchType.EAGER)
    private List<Employee> employees;


    public void addEmployee(Employee employee) {
        Objects.requireNonNull(employee);
        if (employees == null) {
            employees = new ArrayList<>();
        }
        employees.add(employee);
    }


    public void removeEmployee(Employee employee) {
        Objects.requireNonNull(employee);
        if (employees == null) {
            return;
        }
        employees.removeIf(c -> Objects.equals(c.getId(), employee.getId()));
    }
}