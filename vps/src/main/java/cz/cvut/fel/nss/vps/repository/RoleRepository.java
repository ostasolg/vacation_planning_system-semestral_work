package cz.cvut.fel.nss.vps.repository;

import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(String name);
}