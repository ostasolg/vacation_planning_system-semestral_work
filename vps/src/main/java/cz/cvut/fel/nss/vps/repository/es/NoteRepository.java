package cz.cvut.fel.nss.vps.repository.es;

import cz.cvut.fel.nss.vps.model.es.Note;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends ElasticsearchRepository<Note, String> {

    Iterable<Note> findAllByTextContaining(String pattern);

    Iterable<Note> findAllByAuthor(String author);

}