package cz.cvut.fel.nss.vps.patterns.facade;

import cz.cvut.fel.nss.vps.model.StateHoliday;
import cz.cvut.fel.nss.vps.repository.StateHolidayRepository;
import cz.cvut.fel.nss.vps.service.StateHolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
public class StateHolidayFacade {


    @Autowired
    private StateHolidayService stateHolidayService;



    public List<StateHoliday> getAllStateHolidays() {

        return stateHolidayService.getAllStateHolidays();
    }


    public StateHoliday getStateHolidayById(Long id) {

        return stateHolidayService.loadDataOfStateHoliday(id);
    }


    public StateHoliday createStateHoliday(StateHoliday stateHoliday) {

         return stateHolidayService.create(stateHoliday);
    }


    public StateHoliday updateStateHoliday(Long id, StateHoliday newStateHoliday) {

        StateHoliday oldStateHoliday = stateHolidayService.loadDataOfStateHoliday(id);
        StateHoliday updatedStateHoliday = stateHolidayService.update(oldStateHoliday, newStateHoliday);
        return updatedStateHoliday;
    }


    public StateHoliday removeStateHoliday(Long id) {

        StateHoliday stateHoliday = stateHolidayService.loadDataOfStateHoliday(id);
        stateHolidayService.remove(stateHoliday);
        return stateHoliday;
    }
}