package cz.cvut.fel.nss.vps.service;


import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.exceptions.ValidationException;
import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Role;
import cz.cvut.fel.nss.vps.repository.EmployeeRepository;
import cz.cvut.fel.nss.vps.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class RoleService {


    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EmployeeRepository employeeRepository;


    @Cacheable(value = "roles")
    @Transactional
    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }


    @CacheEvict(value = {"roles", "teams", "employees", "holidayRequests"}, allEntries = true)
    @Transactional
    public Role create(@NotNull Role role) {
        return roleRepository.save(role);
    }


    @CacheEvict(value = {"roles", "teams", "employees", "holidayRequests"}, allEntries = true)
    @Transactional
    public Role update(@NotNull Role oldRole, @NotNull Role newRole) throws ValidationException {

        Objects.requireNonNull(newRole.getName());
        oldRole.setName(newRole.getName());
        return roleRepository.save(oldRole);
    }


    @CacheEvict(value = {"roles", "teams", "employees", "holidayRequests"}, allEntries = true)
    @Transactional
    public void remove(@NotNull Role role) throws ValidationException {

        List<Employee> employeesWithRole = employeeRepository.findEmployeesByRoleName(role.getName());
        if (!employeesWithRole.isEmpty()) {
            throw new ValidationException("The role cannot be removed because other employees use it.");
        }
        roleRepository.delete(role);
    }


    @Transactional
    public Role loadDataOfRole(@NotNull Long id) throws NotFoundException {
        Optional<Role> roleData = roleRepository.findById(id);
        if (roleData.isPresent()) {
            return roleData.get();
        }
        throw NotFoundException.create("Role", id);
    }
}
