package cz.cvut.fel.nss.vps.controller.es;

import cz.cvut.fel.nss.vps.model.es.Note;
import cz.cvut.fel.nss.vps.repository.es.NoteRepository;
import cz.cvut.fel.nss.vps.service.es.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;



@RestController
@RequestMapping("/api/es/notes")
public class NoteController {

    @Autowired
    NoteRepository noteRepository;
    @Autowired
    NoteService noteService;

    private static final Logger LOG = LoggerFactory.getLogger(NoteController.class);


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllNotes() {

        List<Note> notes = StreamSupport.stream(noteRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        if (notes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(notes, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/find_by_text/{text}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAllNotesByText(@PathVariable(name = "text") String searchedText) {

        List<Note> notes = StreamSupport.stream(
                noteRepository.findAllByTextContaining(searchedText).spliterator(), false)
                .collect(Collectors.toList());
        if (notes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(notes, HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/find_by_author/{author}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAllNotesByAuthor(@PathVariable(name = "author") String username) {

        List<Note> notes = StreamSupport.stream(
                noteRepository.findAllByAuthor(username).spliterator(), false)
                .collect(Collectors.toList());
        if (notes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(notes, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createNote(@RequestBody Note note) {

        note = noteService.createNote(note);
        LOG.debug("Note {} was successfully created.", note);
        return new ResponseEntity<>(note, HttpStatus.CREATED);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateNote(@PathVariable("id") String id, @RequestBody Note newNote) {

        Note oldNote = noteService.loadDataOfNote(id);
        Note updatedNote = noteService.update(oldNote, newNote);
        LOG.debug("Updated note {}.", updatedNote);
        return new ResponseEntity<>(updatedNote, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeNote(@PathVariable("id") String id) {

        noteService.remove(id);
        LOG.debug("Removed note with id {}.", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}