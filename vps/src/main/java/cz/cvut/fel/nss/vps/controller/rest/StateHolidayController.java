package cz.cvut.fel.nss.vps.controller.rest;

import cz.cvut.fel.nss.vps.patterns.facade.StateHolidayFacade;
import cz.cvut.fel.nss.vps.model.StateHoliday;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/stateholidays")
public class StateHolidayController {

    private static final Logger LOG = LoggerFactory.getLogger(StateHolidayController.class);

    @Autowired
    private StateHolidayFacade stateHolidayFacade;



    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllStateHolidays() {

        List<StateHoliday> stateHolidays = stateHolidayFacade.getAllStateHolidays();
        if (stateHolidays.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(stateHolidays, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStateHolidayById(@PathVariable("id") Long id) {

        StateHoliday stateHoliday = stateHolidayFacade.getStateHolidayById(id);
        return new ResponseEntity<>(stateHoliday, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createStateHoliday(@Valid @RequestBody StateHoliday stateHoliday) {

        stateHoliday = stateHolidayFacade.createStateHoliday(stateHoliday);
        LOG.debug("StateHoliday {} was successfully created.", stateHoliday);
        return new ResponseEntity<>(stateHoliday, HttpStatus.CREATED);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateStateHoliday(@PathVariable("id") Long id,
                                                @RequestBody StateHoliday newStateHoliday) {

        StateHoliday updatedStateHoliday = stateHolidayFacade.updateStateHoliday(id,newStateHoliday);
        LOG.debug("Updated StateHoliday {}.", updatedStateHoliday);
        return new ResponseEntity<>(updatedStateHoliday, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeStateHoliday(@PathVariable("id") Long id) {

        StateHoliday stateHoliday = stateHolidayFacade.removeStateHoliday(id);
        LOG.debug("Removed StateHoliday {}.", stateHoliday);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}