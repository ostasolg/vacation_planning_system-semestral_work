package cz.cvut.fel.nss.vps.controller.rest;


import cz.cvut.fel.nss.vps.patterns.facade.HolidayRequestFacade;
import cz.cvut.fel.nss.vps.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
@RequestMapping("/api/holidayrequests")
public class HolidayRequestController {

    private static final Logger LOG = LoggerFactory.getLogger(HolidayRequestController.class);

    @Autowired
    private HolidayRequestFacade holidayRequestFacade;



    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/find_by_employee/{employee_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findHolidayRequestsByEmployeeId(@PathVariable("employee_id") Long employee_id) {

        List<HolidayRequest> holidayRequests = holidayRequestFacade.findHolidayRequestsByEmployeeId(employee_id);
        if (holidayRequests.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(holidayRequests, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/find_by_team/{team_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAllHolidayRequestsByTeamId(@PathVariable("team_id") Long team_id) {

        List<HolidayRequest> holidayRequests = holidayRequestFacade.findAllHolidayRequestsByTeamId(team_id);
        if (holidayRequests.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(holidayRequests, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/find_by_state_and_team/{state}/{team_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAllHolidayRequestsByStateAndTeamId( @PathVariable("state") String state,
                                                                   @PathVariable("team_id") Long team_id) {

        List<HolidayRequest> holidayRequests = holidayRequestFacade.findAllHolidayRequestsByStateAndTeamId(
                state, team_id);
        if (holidayRequests.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(holidayRequests, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllHolidayRequests() {

        List<HolidayRequest> holidayRequests = holidayRequestFacade.getAllHolidayRequests();
        if (holidayRequests.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(holidayRequests, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getHolidayRequestById(@PathVariable("id") Long id) {

        HolidayRequest holidayRequest = holidayRequestFacade.getHolidayRequestById(id);
        return new ResponseEntity<>(holidayRequest, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createHolidayRequest(@RequestBody HolidayRequest holidayRequest) {

        holidayRequest = holidayRequestFacade.createHolidayRequest(holidayRequest);
        LOG.debug("HolidayRequest {} was successfully created.", holidayRequest);
        return new ResponseEntity<>(holidayRequest, HttpStatus.CREATED);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> editHolidayRequest(
            @PathVariable("id") Long id, @RequestBody HolidayRequest newHolidayRequest) {

        HolidayRequest updatedHolidayRequest = holidayRequestFacade.editHolidayRequest(id, newHolidayRequest);
        LOG.debug("Updated HolidayRequest {}.", updatedHolidayRequest);
        return new ResponseEntity<>(updatedHolidayRequest, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeHolidayRequest(@PathVariable("id") Long id) {

        HolidayRequest holidayRequest = holidayRequestFacade.removeHolidayRequest(id);
        LOG.debug("Removed HolidayRequest {}.", holidayRequest);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}/approve")
    public ResponseEntity<?> approveHolidayRequest(@PathVariable("id") Long id) {

        HolidayRequest holidayRequest = holidayRequestFacade.approveHolidayRequest(id);
        LOG.debug("Approved HolidayRequest {}.", holidayRequest);
        return new ResponseEntity<>(holidayRequest, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}/reject")
    public ResponseEntity<?> rejectHolidayRequest(@PathVariable("id") Long id) {

        HolidayRequest holidayRequest = holidayRequestFacade.rejectHolidayRequest(id);
        LOG.debug("Rejected HolidayRequest {}.", holidayRequest);
        return new ResponseEntity<>(holidayRequest, HttpStatus.OK);
    }
}