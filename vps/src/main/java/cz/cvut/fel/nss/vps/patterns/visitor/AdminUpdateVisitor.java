package cz.cvut.fel.nss.vps.patterns.visitor;

import cz.cvut.fel.nss.vps.model.Employee;


/**
 * This class represents visitor for updating employee by admin.
 */
public class AdminUpdateVisitor implements Visitor {

    private  Employee newEmployee;


    public void setNewEmployee(Employee newEmployee) {
        this.newEmployee = newEmployee;
    }


    @Override
    public void visit(Employee employee) {

        if (newEmployee.getFirstName() != null) {
            employee.setFirstName(newEmployee.getFirstName());
        }
        if (newEmployee.getLastName() != null) {
            employee.setLastName(newEmployee.getLastName());
        }
        if (newEmployee.getEmail() != null && !newEmployee.getEmail().equals(employee.getEmail())) {
            employee.setEmail(newEmployee.getEmail());
        }
        if (newEmployee.getAuthRole() != null) {
            employee.setAuthRole(newEmployee.getAuthRole());
        }
        if (newEmployee.getNumberOfFreeHolidayDays() != null) {
            employee.setNumberOfFreeHolidayDays(newEmployee.getNumberOfFreeHolidayDays());
        }
    }
}
