package cz.cvut.fel.nss.vps.service;


import cz.cvut.fel.nss.vps.exceptions.EmployeeAccessException;
import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.exceptions.ValidationException;
import cz.cvut.fel.nss.vps.model.AuthRole;
import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Role;
import cz.cvut.fel.nss.vps.patterns.visitor.AdminUpdateVisitor;
import cz.cvut.fel.nss.vps.patterns.visitor.UserUpdateVisitor;
import cz.cvut.fel.nss.vps.repository.RoleRepository;
import cz.cvut.fel.nss.vps.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;


@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private TeamService teamService;
    @Autowired
    private PasswordEncoder passwordEncoder;




    @Cacheable(value = "employees")
    @Transactional
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }


    @CacheEvict(value = {"employees", "teams", "holidayRequests", "roles"}, allEntries = true)
    @Transactional
    public Employee create(@NotNull Employee employee) {

        String newPassword = passwordEncoder.encode(employee.getPassword());
        employee.setPassword(newPassword);
        return employeeRepository.save(employee);
    }


    @CacheEvict(value = {"employees", "teams", "holidayRequests", "roles"}, allEntries = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Employee updateEmployeeController(@NotNull Long id, @NotNull Employee newEmployee)
            throws ValidationException {

        Employee oldEmployee = loadDataOfEmployee(id);

        if (newEmployee.getPassword() != null) {
            oldEmployee.setPassword(passwordEncoder.encode(newEmployee.getPassword()));
        }

        if (Utils.getCurrentAuthRole().equals(AuthRole.ADMIN)) {
            AdminUpdateVisitor adminUpdateVisitor = new AdminUpdateVisitor();
            adminUpdateVisitor.setNewEmployee(newEmployee);
            oldEmployee.accept(adminUpdateVisitor);
        } else {
            UserUpdateVisitor userUpdateVisitor = new UserUpdateVisitor();
            userUpdateVisitor.setNewEmployee(newEmployee);
            oldEmployee.accept(userUpdateVisitor);
        }
        return employeeRepository.save(oldEmployee);
    }


    @CacheEvict(value = {"employees", "teams", "holidayRequests", "roles"}, allEntries = true)
    @Transactional
    public void remove(@NotNull Employee employee) {

       if (employee.getTeam() != null) {
           teamService.removeEmployeeFromTeam(employee, employee.getTeam());
       }
       employeeRepository.delete(employee);
    }


    @CacheEvict(value = {"employees", "teams", "holidayRequests", "roles"}, allEntries = true)
    @Transactional
    public Employee addRoleToEmployee(@NotNull Employee employee, @NotBlank String roleName) {

        employee = employeeRepository.getOne(employee.getId());
        Optional<Role> roleData = roleRepository.findByName(roleName);

        if (roleData.isPresent()) {
            Role role = roleData.get();
            employee.setRole(role);
        } else {
            Role role = new Role();
            role.setName(roleName);
            roleRepository.save(role);
            employee.setRole(role);
        }
        return employeeRepository.save(employee);
    }



    public void validateEmployeeAccess(@NotNull Long id, String errorMessage) throws EmployeeAccessException {

        if (!Utils.getCurrent().getId().equals(id) &&
                Utils.getCurrentAuthRole().equals(AuthRole.USER)) {
            throw new EmployeeAccessException("Employee of auth_role USER has no access to " + errorMessage + ".");
        }
    }

    @Transactional
    public Employee loadDataOfEmployee(@NotNull Long id) throws NotFoundException {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            return employeeData.get();
        }
        throw NotFoundException.create("Employee", id);
    }
}