package cz.cvut.fel.nss.vps.service.kafka;

import cz.cvut.fel.nss.vps.model.kafka.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class KafkaConsumer {

    @Value("${spring.mail.username}")
    private String email;

    @Autowired
    private JavaMailSender emailSender;

    private static final Logger LOG = LoggerFactory.getLogger(KafkaProducer.class);


    /**
     * This function handles Kafka incoming messages. Each incoming message triggers sending of confirmation email to
     * corresponding employee email address.
     *
     * @param kafkaMessage kafka message with employee and email body information
     */
    @KafkaListener(topics = "nss")
    public void listen(KafkaMessage kafkaMessage) {
        sendEmail(kafkaMessage.getRecipient(), "NSS Vacation Planning System", kafkaMessage.getEmailBody());
        LOG.info(String.format("$$ -> Consumed Message -> %s", kafkaMessage));
    }


    public void sendEmail(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(email);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

}