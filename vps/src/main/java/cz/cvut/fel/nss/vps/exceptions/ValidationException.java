package cz.cvut.fel.nss.vps.exceptions;


/**
 * Signifies that invalid data have been provided to the application.
 */
public class ValidationException extends VpsException {

    public ValidationException(String message) {
        super(message);
    }
}
