package cz.cvut.fel.nss.vps.repository;

import cz.cvut.fel.nss.vps.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findByUsername(String username);

    @Query(value = "SELECT e FROM Employee e WHERE ?1 = e.role.name")
    List<Employee> findEmployeesByRoleName(String roleName);

    Boolean existsByUsername(String username);
}
