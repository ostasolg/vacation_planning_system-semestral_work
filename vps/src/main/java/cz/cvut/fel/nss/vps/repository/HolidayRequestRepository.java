package cz.cvut.fel.nss.vps.repository;

import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.HolidayRequest;
import cz.cvut.fel.nss.vps.model.HolidayRequestState;
import cz.cvut.fel.nss.vps.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;



@Repository
public interface HolidayRequestRepository extends JpaRepository<HolidayRequest, Long> {

    List<HolidayRequest> findHolidayRequestsByEmployee(Employee employee);

    @Query(value = "SELECT h FROM HolidayRequest h WHERE ?1 = h.employee.team")
    List<HolidayRequest> findByTeam(Team team);

    @Query(value = "SELECT h FROM HolidayRequest h WHERE ?1 = h.state and ?2 = h.employee.team")
    List<HolidayRequest> findByStateAndTeam(HolidayRequestState state, Team team);
}
