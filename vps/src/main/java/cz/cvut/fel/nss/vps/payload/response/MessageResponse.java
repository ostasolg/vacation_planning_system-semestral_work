package cz.cvut.fel.nss.vps.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.management.ConstructorParameters;

/**
 * Contains information about an error and can be send to client as JSON to let them know what went wrong.
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class MessageResponse {

    private final String message;
    private String requestUri;

    @Override
    public String toString() {
        return "ErrorInfo{" + requestUri + ", message = " + message + "}";
    }
}