package cz.cvut.fel.nss.vps.repository;

import cz.cvut.fel.nss.vps.model.StateHoliday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface StateHolidayRepository extends JpaRepository<StateHoliday, Long> {

    @Query(value = "SELECT s.date FROM StateHoliday s")
    List<LocalDate> findAllStateHolidayDates();
}
