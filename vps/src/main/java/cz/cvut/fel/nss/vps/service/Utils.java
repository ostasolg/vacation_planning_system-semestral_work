package cz.cvut.fel.nss.vps.service;

import cz.cvut.fel.nss.vps.model.AuthRole;
import cz.cvut.fel.nss.vps.model.HolidayRequest;
import cz.cvut.fel.nss.vps.model.kafka.KafkaMessage;
import cz.cvut.fel.nss.vps.patterns.observer.HolidayRequestObserver;
import cz.cvut.fel.nss.vps.security.UserDetailsImpl;
import cz.cvut.fel.nss.vps.service.kafka.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

@Service
public class Utils {


    public static UserDetailsImpl getCurrent() {
        return (UserDetailsImpl) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
    }


    public static AuthRole getCurrentAuthRole() {

        String authRole = getCurrent().getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()).get(0);

        if (authRole.equals(AuthRole.ADMIN.toString())) {
            return AuthRole.ADMIN;
        }
        return AuthRole.USER;
    }


    public static String getCurrentEmail() {
        return getCurrent().getEmail();
    }
}
