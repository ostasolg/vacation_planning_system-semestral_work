package cz.cvut.fel.nss.vps.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@Table(name = "holidayRequests")
public class HolidayRequest extends AbstractEntity {


    @NotNull(message = "The date of the beginning of the holiday request may not be null.")
    private LocalDate dateFrom;

    @NotNull(message = "The date of the end of the holiday request may not be null.")
    private LocalDate dateTo;

    @NotNull(message = "State of the holiday request may not be null.")
    private HolidayRequestState state;


    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne
    @JoinColumn(name = "employee", nullable = false)
    private Employee employee;



    public Long getNumberOfBusinessDays(Optional<List<LocalDate>> stateHolidays) {

        if (dateFrom == null || dateTo == null) {
            throw new IllegalArgumentException("Invalid method argument(s) to countBusinessDays.");
        }
        Predicate<LocalDate> isHoliday = date -> stateHolidays.isPresent() ? stateHolidays.get().contains(date) : false;

        Predicate<LocalDate> isWeekend = date -> date.getDayOfWeek() == DayOfWeek.SATURDAY
                || date.getDayOfWeek() == DayOfWeek.SUNDAY;

        long daysBetween = ChronoUnit.DAYS.between(dateFrom, dateTo);

        long businessDays = Stream.iterate(dateFrom, date -> date.plusDays(1)).limit(daysBetween+1)
                .filter(isHoliday.or(isWeekend).negate()).count();

        return businessDays;
    }



}