package cz.cvut.fel.nss.vps.exceptions;

public class EmployeeAccessException extends VpsException{

    public EmployeeAccessException(String message) {
        super(message);
    }
}
