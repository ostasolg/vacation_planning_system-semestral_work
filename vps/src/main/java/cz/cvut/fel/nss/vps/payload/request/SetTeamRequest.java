package cz.cvut.fel.nss.vps.payload.request;

import lombok.Data;
import javax.validation.constraints.NotNull;


@Data
public class SetTeamRequest {

    @NotNull
    private Long empId;
}
