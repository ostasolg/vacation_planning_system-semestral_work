package cz.cvut.fel.nss.vps.service;

import cz.cvut.fel.nss.vps.exceptions.EmployeeAccessException;
import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.exceptions.ValidationException;
import cz.cvut.fel.nss.vps.model.AuthRole;
import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Team;
import cz.cvut.fel.nss.vps.repository.EmployeeRepository;
import cz.cvut.fel.nss.vps.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;



@Service
public class TeamService {


    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private TeamRepository teamRepository;


    @Cacheable(value = "teams")
    @Transactional
    public List<Team> getAllTeams() {
        return teamRepository.findAll();
    }


    @CacheEvict(value = {"teams", "holidayRequests", "employees", "roles"}, allEntries = true)
    @Transactional
    public Team create(@NotNull Team team) {
        return teamRepository.save(team);
    }


    @CacheEvict(value = {"teams", "holidayRequests", "employees", "roles"}, allEntries = true)
    @Transactional
    public void remove(@NotNull Team team) throws ValidationException {

        if (team.getEmployees() != null && team.getEmployees().size() != 0) {
            throw new ValidationException("This team cannot be removed because it contains employees.");
        }
        teamRepository.delete(team);
    }


    @CacheEvict(value = {"teams", "holidayRequests", "employees", "roles"}, allEntries = true)
    @Transactional
    public Team update(@NotNull Team oldTeam, @NotNull Team newTeam) throws ValidationException {

        Objects.requireNonNull(newTeam.getName());
        oldTeam.setName(newTeam.getName());
        return teamRepository.save(oldTeam);
    }


    @CacheEvict(value = {"teams", "holidayRequests", "employees", "roles"}, allEntries = true)
    @Transactional
    public Team addEmployeeToTeam(@NotNull Long empId, @NotNull Team team) throws ValidationException {

        Employee employee = employeeRepository.getOne(empId);
        if (team.getEmployees()!= null && team.getEmployees().contains(employee)) {
            return team;
        }
        else if (employee.getTeam() != null && !employee.getTeam().getId().equals(team.getId())) {
            throw new ValidationException("The employee is in another team.");
        }
        team.addEmployee(employee);
        employee.setTeam(team);
        employeeRepository.save(employee);
        return teamRepository.save(team);
    }


    @CacheEvict(value = {"teams", "holidayRequests", "employees", "roles"}, allEntries = true)
    @Transactional
    public Team removeEmployeeFromTeam(@NotNull Employee employee, @NotNull Team team) {

        employee = employeeRepository.getOne(employee.getId());
        if (employee.getTeam() != null && team.getEmployees().contains(employee)) {
            team.removeEmployee(employee);
            employee.setTeam(null);
        } else {
            throw new ValidationException("Employee " + employee.getUsername()
                    + " is not in team " + team.getName() + ".");
        }
        if (employee.getRole() != null) {
            employee.setRole(null);
        }
        employeeRepository.save(employee);
        return teamRepository.save(team);
    }


    public void validateEmployeeAccess(@NotNull Team team) throws EmployeeAccessException {

        boolean validationValue = team.getEmployees().stream().
                anyMatch(employee -> employee.getId().equals(Utils.getCurrent().getId()));

        if (validationValue || Utils.getCurrentAuthRole().equals(AuthRole.ADMIN)) {
            return;
        }
        throw new EmployeeAccessException("Employee of auth_role USER has no access to other teams.");
    }


    @Transactional
    public Team loadDataOfTeam(@NotNull Long id) throws NotFoundException {
        Optional<Team> teamData = teamRepository.findById(id);
        if (teamData.isPresent()) {
            return teamData.get();
        }
        throw NotFoundException.create("Team", id);
    }
}