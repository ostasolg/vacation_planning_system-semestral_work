package cz.cvut.fel.nss.vps.patterns.observer;

import cz.cvut.fel.nss.vps.model.HolidayRequest;
import cz.cvut.fel.nss.vps.model.kafka.KafkaMessage;
import cz.cvut.fel.nss.vps.service.kafka.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This class represents the holiday requests observer.
 */
@Component
@Scope("prototype")
public class HolidayRequestObserver implements Observer {

    private HolidayRequest holidayRequest;

    @Autowired
    private KafkaProducer kafkaProducer;

    /**
     * This method sends an email to the owner of the holiday request.
     */
    @Override
    public void update() {
        String emailMessage = "Your holiday request from " + holidayRequest.getDateFrom() + " to " +
                holidayRequest.getDateTo() + " was " + holidayRequest.getState() + ".";

        KafkaMessage kafkaMessage = new KafkaMessage(holidayRequest.getEmployee().getEmail(), emailMessage);
        kafkaProducer.sendMessage(kafkaMessage);
    }


    /**
     * This method sets the HolidayRequest instance.
     */
    public void setEntity(HolidayRequest holidayRequest) {
        this.holidayRequest = holidayRequest;
        holidayRequest.attach(this);
    }
}
