package cz.cvut.fel.nss.vps.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.fel.nss.vps.patterns.observer.Observer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @Transient
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Observer> observers = new ArrayList<>();


    /**
     * This method attaches observer.
     * @param observer Observer
     */
    public void attach(Observer observer){
        observers.add(observer);
    }


    /**
     * This method notifies all observers.
     */
    public void notifyAllObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
