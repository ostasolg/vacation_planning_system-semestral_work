package cz.cvut.fel.nss.vps.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.fel.nss.vps.patterns.visitor.Visitor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Data
@NoArgsConstructor
@Entity
@Table(name = "employees",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class Employee extends AbstractEntity {

    @NotBlank
    @Size(min = 1, max = 30, message = "Username must be between 1 and 30 characters.")
    @OrderBy
    private String username;

    @NotBlank
    @Size(min = 3, max = 120, message = "Password must be between 3 and 120 characters.")
    private String password;

    @NotBlank
    @Size(min = 1, max = 50, message = "First name must be between 1 and 50 characters.")
    private String firstName;

    @NotBlank
    @Size(min = 1, max = 50, message = "Last name must be between 1 and 50 characters.")
    private String lastName;

    @NotBlank
    @Size(min = 5, max = 50, message = "Email must be between 5 and 50 characters.")
    @Email
    private String email;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Authorisation role may not be null.")
    private AuthRole authRole;

    @PositiveOrZero(message =  "Number of free holiday days of each employee cannot be negative.")
    private Integer numberOfFreeHolidayDays = 0;

    @ToString.Exclude
    @OneToMany(mappedBy = "employee", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<HolidayRequest> holidayRequests;


    @ManyToOne
    @JoinColumn(name = "team")
    @JsonIgnore
    private Team team;

    @ManyToOne
    @JoinColumn(name = "role")
    private Role role;


    /**
     * This method accepts visitor.
     * @param visitor Visitor
     */
    public void accept(Visitor visitor){
        visitor.visit(this);
    }


    public void addHolidayRequest(HolidayRequest holidayRequest) {
        Objects.requireNonNull(holidayRequest);
        if (holidayRequests == null) {
            this.holidayRequests = new ArrayList<>();
        }
        holidayRequests.add(holidayRequest);
    }

    public void removeHolidayRequest(HolidayRequest holidayRequest) {
        Objects.requireNonNull(holidayRequest);
        if (holidayRequests == null) {
            return;
        }
        holidayRequests.removeIf(c -> Objects.equals(c.getId(), holidayRequest.getId()));
    }

    public boolean doesEmployeeHaveEnoughHolidayDays(Integer number) {
        return numberOfFreeHolidayDays >= number;
    }

    public boolean isHolidayRequestWithoutOverlaps(HolidayRequest holidayRequest) {

        boolean result = true;

        if (holidayRequests == null) {
            return result;
        }

        for (HolidayRequest hr : holidayRequests) {
            if (hr.getDateFrom().isEqual(holidayRequest.getDateFrom()) ||
                    hr.getDateTo().isEqual(holidayRequest.getDateFrom()) ||
                    hr.getDateFrom().isEqual(holidayRequest.getDateTo()) ||
                    hr.getDateTo().isEqual(holidayRequest.getDateTo()))  {


                result = false;
                break;
            }

            if (holidayRequest.getDateFrom().isAfter(hr.getDateFrom())
                    && holidayRequest.getDateFrom().isBefore(hr.getDateTo())) {

                result = false;
                break;
            }

            if (holidayRequest.getDateFrom().isBefore(hr.getDateFrom())
                    && holidayRequest.getDateTo().isAfter(hr.getDateFrom())) {


                result = false;
                break;
            }
        }
        return result;
    }

    public void addNumberOfHolidays(Integer days) {
        if (days > 0) {
            numberOfFreeHolidayDays += days;
        }
    }

    public void subtractNumberOfHolidays(Integer days) {
        if (days > 0) {
            numberOfFreeHolidayDays -= days;}
    }
}