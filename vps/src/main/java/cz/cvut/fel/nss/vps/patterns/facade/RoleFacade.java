package cz.cvut.fel.nss.vps.patterns.facade;

import cz.cvut.fel.nss.vps.model.Role;
import cz.cvut.fel.nss.vps.repository.RoleRepository;
import cz.cvut.fel.nss.vps.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
public class RoleFacade {

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    RoleService roleService;



    public List<Role> getAllRoles() {
        List<Role> roles = roleService.getAllRoles();
        return  roles;

    }


    public Role getRoleById(Long id) {

        Role role = roleService.loadDataOfRole(id);
        return role;
    }


    public Role createRole(Role role) {

        return roleService.create(role);
    }


    public Role updateRole(Long id, Role newRole) {

        Role oldRole = roleService.loadDataOfRole(id);
        Role updatedRole = roleService.update(oldRole, newRole);
        return updatedRole;
    }


    public Role removeRole(Long id) {

        Role role = roleService.loadDataOfRole(id);
        roleService.remove(role);
        return role;
    }
}