package cz.cvut.fel.nss.vps.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;



@Data
@NoArgsConstructor
@Entity
@Table(name = "stateHolidays",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "date")
        })
public class StateHoliday extends AbstractEntity {

    @NotBlank
    @Size(min = 1, max = 30, message = "Name of the state holiday must be between 1 and 30 characters.")
    private String name;

    @NotNull(message = "Date of the state holiday may not be null.")
    private LocalDate date;
}
