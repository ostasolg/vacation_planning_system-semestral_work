package cz.cvut.fel.nss.vps.model.es;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@Document(indexName = "notes")
public class Note {

    @Id
    private String id;

    @NotBlank
    private String text;

    @NotNull
    private String author;
}