package cz.cvut.fel.nss.vps.patterns.visitor;

import cz.cvut.fel.nss.vps.model.Employee;


/**
 * This class represents visitor for updating user's account by himself.
 */
public class UserUpdateVisitor implements Visitor {

    private  Employee newEmployee;


    public void setNewEmployee(Employee newEmployee) {
        this.newEmployee = newEmployee;
    }


    @Override
    public void visit(Employee employee) {
        if (newEmployee.getFirstName() != null) {
            employee.setFirstName(newEmployee.getFirstName());
        }
        if (newEmployee.getLastName() != null) {
            employee.setLastName(newEmployee.getLastName());
        }
        if (newEmployee.getEmail() != null && !newEmployee.getEmail().equals(employee.getEmail())) {
            employee.setEmail(newEmployee.getEmail());
        }

    }
}
