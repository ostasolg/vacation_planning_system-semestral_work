package cz.cvut.fel.nss.vps.service.kafka;

import cz.cvut.fel.nss.vps.model.kafka.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer {

    private final KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    private static final Logger LOG = LoggerFactory.getLogger(KafkaProducer.class);

    /**
     * KafkaProducer class constructor.
     *
     * @param kafkaTemplate autowired KafkaTemplate object
     */
    @Autowired
    public KafkaProducer(KafkaTemplate<String, KafkaMessage> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * This function inserts new kafka message into kafka topic.
     *
     * @param kafkaMessage kafka message to be inserted
     */
    public void sendMessage(KafkaMessage kafkaMessage) {
        String TOPIC = "nss";
        kafkaTemplate.send(TOPIC, kafkaMessage);
        LOG.info(String.format("$$ -> Producing message --> %s", kafkaMessage));
    }

}