package cz.cvut.fel.nss.vps.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
@NoArgsConstructor
@Entity
@Table(name = "roles", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})

public class Role extends AbstractEntity {

    @NotBlank
    @Size(min = 1, max = 30, message = "Name of the role must be between 1 and 30 characters.")
    private String name;

}
