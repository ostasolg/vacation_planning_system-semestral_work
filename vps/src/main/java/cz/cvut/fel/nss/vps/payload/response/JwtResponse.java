package cz.cvut.fel.nss.vps.payload.response;

import lombok.Data;


@Data
public class JwtResponse {

    private final String accessToken;
    private final String type = "Bearer";
    private final String username;
    private final String authRole;
    private final Long id;

    public JwtResponse(String accessToken, String username, String authRole, Long id) {
        this.accessToken = accessToken;
        this.username = username;
        this.authRole = authRole;
        this.id = id;
    }
}