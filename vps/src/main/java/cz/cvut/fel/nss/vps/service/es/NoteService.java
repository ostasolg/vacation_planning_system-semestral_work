package cz.cvut.fel.nss.vps.service.es;


import cz.cvut.fel.nss.vps.exceptions.EmployeeAccessException;
import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.model.es.Note;
import cz.cvut.fel.nss.vps.repository.es.NoteRepository;
import cz.cvut.fel.nss.vps.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;


@Service
public class NoteService {


    @Autowired
    private NoteRepository noteRepository;


    @Transactional
    public Note createNote(@NotNull Note note) {

        note.setAuthor(Utils.getCurrent().getUsername());
        return noteRepository.save(note);
    }


    @Transactional
    public Note update(@NotNull Note oldNote, @NotNull Note newNote) {

        validateEmployeeAccess(oldNote.getAuthor(), "update");
        Objects.requireNonNull(newNote.getText());
        oldNote.setText(newNote.getText());
        return noteRepository.save(oldNote);
    }


    @Transactional
    public void remove(@NotNull String id) {

        Note note = loadDataOfNote(id);
        validateEmployeeAccess(note.getAuthor(), "remove");
        noteRepository.delete(note);
    }


    @Transactional
    public Note loadDataOfNote(@NotNull String id) throws NotFoundException {

        Optional<Note> noteData = noteRepository.findById(id);
        if (noteData.isPresent()) {
            return noteData.get();
        }
        throw NotFoundException.create("Note", id);
    }


    public void validateEmployeeAccess(@NotNull String author, String errorMessage) throws EmployeeAccessException {

        if (!Utils.getCurrent().getUsername().equals(author)) {
            throw new EmployeeAccessException("The employee cannot " + errorMessage + " notes of other authors.");
        }
    }
}
