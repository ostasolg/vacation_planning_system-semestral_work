package cz.cvut.fel.nss.vps.patterns.observer;


/**
 * This is an interface for the observer.
 */
public interface Observer {

    /**
     * This method updates instance.
     */
    void update();
}