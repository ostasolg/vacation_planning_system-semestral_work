package cz.cvut.fel.nss.vps.repository;

import cz.cvut.fel.nss.vps.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    Optional<Team> findTeamByName(String name);

    Boolean existsByName(String name);
}
