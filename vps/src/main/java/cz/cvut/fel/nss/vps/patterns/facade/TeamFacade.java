package cz.cvut.fel.nss.vps.patterns.facade;

import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Team;
import cz.cvut.fel.nss.vps.payload.request.SetTeamRequest;
import cz.cvut.fel.nss.vps.repository.TeamRepository;
import cz.cvut.fel.nss.vps.service.EmployeeService;
import cz.cvut.fel.nss.vps.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
public class TeamFacade {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TeamService teamService;
    @Autowired
    private EmployeeService employeeService;


    public List<Team> getAllTeams() {

        return teamService.getAllTeams();
    }


    public Team getTeamByName(String name) throws NotFoundException {

        if (teamRepository.existsByName(name)) {
            return teamRepository.findTeamByName(name).get();
        } else {
            throw NotFoundException.create("Team", name);
        }
    }


    public Team getTeamById(Long id) {

        Team team = teamService.loadDataOfTeam(id);
        teamService.validateEmployeeAccess(team);
        return team;
    }


    public Team createTeam(Team team) {

        return teamService.create(team);
    }


    public Team updateTeam(Long id, Team newTeam) {

        Team oldTeam = teamService.loadDataOfTeam(id);
        return teamService.update(oldTeam, newTeam);
    }


    public Team removeTeam(Long id) {

        Team team = teamService.loadDataOfTeam(id);
        teamService.remove(team);
        return team;
    }


    public Team addEmployeeToTeam(Long id, SetTeamRequest setTeamRequest) {

        Team team = teamService.loadDataOfTeam(id);
        return teamService.addEmployeeToTeam(setTeamRequest.getEmpId(), team);
    }


    public Team removeEmployeeFromTeam(Long id, Long empId) {

        Team team = teamService.loadDataOfTeam(id);
        Employee employee = employeeService.loadDataOfEmployee(empId);
        return teamService.removeEmployeeFromTeam(employee, team);
    }
}
