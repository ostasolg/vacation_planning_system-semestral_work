package cz.cvut.fel.nss.vps.patterns.facade;

import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Team;
import cz.cvut.fel.nss.vps.payload.request.SetRoleRequest;
import cz.cvut.fel.nss.vps.repository.EmployeeRepository;
import cz.cvut.fel.nss.vps.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
public class EmployeeFacade {


    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private EmployeeService employeeService;


    public List<Employee> getAllEmployees() {

        return employeeService.getAllEmployees();
    }

    public Employee getEmployeeById(Long id) {

        employeeService.validateEmployeeAccess(id, "data of another employee");
        return employeeService.loadDataOfEmployee(id);
    }


    public Team getTeamByEmployeeId(Long id) {

        employeeService.validateEmployeeAccess(id, "team of another employee");
        Employee employee = employeeService.loadDataOfEmployee(id);
        if (employee.getTeam() != null) {
            return employee.getTeam();
        }
        return null;
    }


    public Employee createEmployee(Employee employee) {

        return employeeService.create(employee);
    }


    public Employee updateEmployee(Long id, Employee newEmployee) {

        employeeService.validateEmployeeAccess(id, "data of another employee and cannot edit it");
        return employeeService.updateEmployeeController(id, newEmployee);
    }


    public Employee removeEmployee(Long id) {

        Employee employee = employeeService.loadDataOfEmployee(id);
        employeeService.remove(employee);
        return employee;
    }


    public Employee getEmployeeByUserName(String username) throws NotFoundException {

        if (employeeRepository.existsByUsername(username)) {
            return employeeRepository.findByUsername(username).get();
        } else {
            throw NotFoundException.create("Employee", username);
        }
    }


    public List<Employee> findEmployeesByRoleName(String roleName) {

        return employeeRepository.findEmployeesByRoleName(roleName);
    }


    public Employee addRoleToEmployee(Long id, SetRoleRequest setRoleRequest) {

        Employee employee = employeeService.loadDataOfEmployee(id);
        return employeeService.addRoleToEmployee(employee, setRoleRequest.getRoleName());
    }
}
