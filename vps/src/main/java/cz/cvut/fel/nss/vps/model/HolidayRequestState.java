package cz.cvut.fel.nss.vps.model;


public enum HolidayRequestState {

    APPROVED("APPROVED"), REJECTED("REJECTED"), PENDING("PENDING");

    private final String name;

    HolidayRequestState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
