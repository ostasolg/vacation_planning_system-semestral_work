package cz.cvut.fel.nss.vps.patterns.visitor;


import cz.cvut.fel.nss.vps.model.Employee;

/**
 * Interface of design pattern Visitor.
 */
public interface Visitor {

    void visit(Employee employee);
}
