package cz.cvut.fel.nss.vps.patterns.facade;

import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.HolidayRequest;
import cz.cvut.fel.nss.vps.model.HolidayRequestState;
import cz.cvut.fel.nss.vps.model.Team;
import cz.cvut.fel.nss.vps.model.kafka.KafkaMessage;
import cz.cvut.fel.nss.vps.repository.HolidayRequestRepository;
import cz.cvut.fel.nss.vps.service.EmployeeService;
import cz.cvut.fel.nss.vps.service.HolidayRequestService;
import cz.cvut.fel.nss.vps.service.TeamService;
import cz.cvut.fel.nss.vps.service.Utils;
import cz.cvut.fel.nss.vps.service.kafka.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
public class HolidayRequestFacade {


    @Autowired
    private HolidayRequestRepository holidayRequestRepository;
    @Autowired
    private HolidayRequestService holidayRequestService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private TeamService teamService;


    public List<HolidayRequest> findHolidayRequestsByEmployeeId(Long employee_id) {

        employeeService.validateEmployeeAccess(employee_id, "holiday requests of other employees");
        Employee employee = employeeService.loadDataOfEmployee(employee_id);
        return holidayRequestRepository.findHolidayRequestsByEmployee(employee);
    }


    public List<HolidayRequest> findAllHolidayRequestsByTeamId(Long team_id) {

        Team team = teamService.loadDataOfTeam(team_id);
        teamService.validateEmployeeAccess(team);
        return holidayRequestService.findAllHolidayRequestsByTeamId(team);
    }


    public List<HolidayRequest> findAllHolidayRequestsByStateAndTeamId(String state, Long team_id) {

        holidayRequestService.validateHolidayRequestState(state);
        Team team = teamService.loadDataOfTeam(team_id);
        return holidayRequestRepository.findByStateAndTeam(HolidayRequestState.valueOf(state), team);
    }


    public List<HolidayRequest> getAllHolidayRequests() {

        return holidayRequestService.getAllHolidayRequests();
    }


    public HolidayRequest getHolidayRequestById(Long id) {

        holidayRequestService.validateWhetherEmpCanGetHR(id, "holiday requests of other employees");
        return holidayRequestService.loadDataOfHolidayRequest(id);
    }


    public HolidayRequest createHolidayRequest(HolidayRequest holidayRequest) {

        return holidayRequestService.create(holidayRequest);
    }


    public HolidayRequest editHolidayRequest(Long id, HolidayRequest newHolidayRequest) {

        holidayRequestService.validateWhetherEmpCanEditOrRemoveHR(id, "edit");
        HolidayRequest oldHolidayRequest = holidayRequestService.loadDataOfHolidayRequest(id);
        return holidayRequestService.editHolidayRequest(oldHolidayRequest, newHolidayRequest);
    }


    public HolidayRequest removeHolidayRequest(Long id) {

        holidayRequestService.validateWhetherEmpCanEditOrRemoveHR(id, "remove");
        HolidayRequest holidayRequest = holidayRequestService.loadDataOfHolidayRequest(id);
        holidayRequestService.remove(holidayRequest);
        return holidayRequest;
    }


    public HolidayRequest approveHolidayRequest(Long id) {

        HolidayRequest holidayRequest = holidayRequestService.loadDataOfHolidayRequest(id);
        holidayRequest = holidayRequestService.approveHolidayRequest(holidayRequest);
        holidayRequest.notifyAllObservers();
        return holidayRequest;
    }


    public HolidayRequest rejectHolidayRequest(Long id) {

        HolidayRequest holidayRequest = holidayRequestService.loadDataOfHolidayRequest(id);
        holidayRequest = holidayRequestService.rejectHolidayRequest(holidayRequest);
        holidayRequest.notifyAllObservers();
        return holidayRequest;
    }
}
