package cz.cvut.fel.nss.vps.service;

import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.exceptions.ValidationException;
import cz.cvut.fel.nss.vps.model.StateHoliday;
import cz.cvut.fel.nss.vps.repository.StateHolidayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class StateHolidayService {

    @Autowired
    StateHolidayRepository stateHolidayRepository;


    @Cacheable(value = "stateHolidays")
    @Transactional
    public List<StateHoliday> getAllStateHolidays() {
        return stateHolidayRepository.findAll();
    }


    @CacheEvict(value = "stateHolidays", allEntries = true)
    @Transactional
    public StateHoliday create(@NotNull StateHoliday stateHoliday) {
        return stateHolidayRepository.save(stateHoliday);
    }


    @CacheEvict(value = "stateHolidays", allEntries = true)
    @Transactional
    public void remove(@NotNull StateHoliday stateHoliday) throws ValidationException {
        stateHolidayRepository.delete(stateHoliday);
    }


    @CacheEvict(value = "stateHolidays", allEntries = true)
    @Transactional
    public StateHoliday update(@NotNull StateHoliday oldStateHoliday, @NotNull StateHoliday newStateHoliday)
            throws ValidationException {

        Objects.requireNonNull(newStateHoliday.getName());
        Objects.requireNonNull(newStateHoliday.getDate());
        oldStateHoliday.setDate(newStateHoliday.getDate());
        oldStateHoliday.setName(newStateHoliday.getName());
        return stateHolidayRepository.save(oldStateHoliday);
    }


    @Transactional
    public StateHoliday loadDataOfStateHoliday(@NotNull Long id) throws NotFoundException {

        Optional<StateHoliday> stateHolidayData = stateHolidayRepository.findById(id);
        if (stateHolidayData.isPresent()) {
            return stateHolidayData.get();
        }
        throw NotFoundException.create("StateHoliday", id);
    }
}
