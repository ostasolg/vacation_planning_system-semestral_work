package cz.cvut.fel.nss.vps.patterns.factory;

import cz.cvut.fel.nss.vps.model.*;
import cz.cvut.fel.nss.vps.repository.*;
import cz.cvut.fel.nss.vps.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.List;


@Component
public class VpsFactory {


    @Autowired
    HolidayRequestRepository holidayRequestRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    StateHolidayRepository stateHolidayRepository;
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    EmployeeRepository employeeRepository;



    public Employee makeUser(String firstname, String lastname, String username, String password, String email,
                         Role role, Integer numberOfFreeHolidayDays) {

        Employee employee = new Employee();
        employee.setFirstName(firstname);
        employee.setLastName(lastname);
        employee.setPassword(password);
        employee.setUsername(username);
        employee.setEmail(email);
        employee.setRole(role);
        employee.setAuthRole(AuthRole.USER);
        employee.setNumberOfFreeHolidayDays(numberOfFreeHolidayDays);
        return employeeService.create(employee);
    }


    public Employee makeAdmin(String firstname, String lastname, String username, String password, String email,
                              Role role, Integer numberOfFreeHolidayDays) {

        Employee employee = new Employee();
        employee.setFirstName(firstname);
        employee.setLastName(lastname);
        employee.setPassword(password);
        employee.setUsername(username);
        employee.setEmail(email);
        employee.setRole(role);
        employee.setAuthRole(AuthRole.ADMIN);
        employee.setNumberOfFreeHolidayDays(numberOfFreeHolidayDays);
        return employeeService.create(employee);
    }


    public HolidayRequest makeHRPending(Employee employee, LocalDate dateFrom, LocalDate dateTo ) {

        HolidayRequest holidayRequest = new HolidayRequest();
        holidayRequest.setDateFrom(dateFrom);
        holidayRequest.setDateTo(dateTo);
        holidayRequest.setState(HolidayRequestState.PENDING);
        holidayRequest.setEmployee(employee);
        return holidayRequestRepository.save(holidayRequest);
    }


    public HolidayRequest makeHRApproved(Employee employee, LocalDate dateFrom, LocalDate dateTo ) {

        HolidayRequest holidayRequest = new HolidayRequest();
        holidayRequest.setDateFrom(dateFrom);
        holidayRequest.setDateTo(dateTo);
        holidayRequest.setState(HolidayRequestState.APPROVED);
        holidayRequest.setEmployee(employee);
        return holidayRequestRepository.save(holidayRequest);
    }


    public HolidayRequest makeHRRejected(Employee employee, LocalDate dateFrom, LocalDate dateTo ) {

        HolidayRequest holidayRequest = new HolidayRequest();
        holidayRequest.setDateFrom(dateFrom);
        holidayRequest.setDateTo(dateTo);
        holidayRequest.setState(HolidayRequestState.REJECTED);
        holidayRequest.setEmployee(employee);
        return holidayRequestRepository.save(holidayRequest);
    }


    public Role makeRole(String name) {

        Role role = new Role();
        role.setName(name);
        return roleRepository.save(role);
    }


    public Team makeTeam(String name, List<Employee> employeeList) {

        Team team = new Team();
        team.setName(name);
        team.setEmployees(employeeList);
        team = teamRepository.save(team);
        for (Employee emp: employeeList) {
            emp.setTeam(team);
            employeeRepository.save(emp);
        }
        return team;
    }


    public StateHoliday makeStateHoliday(String name, LocalDate date) {

        StateHoliday stateHoliday = new StateHoliday();
        stateHoliday.setName(name);
        stateHoliday.setDate(date);
        return stateHolidayRepository.save(stateHoliday);
    }
}