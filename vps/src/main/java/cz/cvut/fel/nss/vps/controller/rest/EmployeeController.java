package cz.cvut.fel.nss.vps.controller.rest;

import cz.cvut.fel.nss.vps.patterns.facade.EmployeeFacade;
import cz.cvut.fel.nss.vps.model.Employee;
import cz.cvut.fel.nss.vps.model.Team;
import cz.cvut.fel.nss.vps.payload.request.SetRoleRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;



@RestController
@RequestMapping("/api/employees")
public class EmployeeController {


    @Autowired
    private EmployeeFacade employeeFacade;

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllEmployees() {

        List<Employee> employees = employeeFacade.getAllEmployees();
        if (employees.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(employees, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEmployeeById(@PathVariable("id") Long id) {

        Employee employee = employeeFacade.getEmployeeById(id);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }



    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}/team", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeamByEmployeeId(@PathVariable("id") Long id) {

        Team team = employeeFacade.getTeamByEmployeeId(id);
        if (team != null) {
            return new ResponseEntity<>(team, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createEmployee(@Valid @RequestBody Employee employee) {

        employee = employeeFacade.createEmployee(employee);
        LOG.debug("Employee {} was successfully registered.", employee);
        return new ResponseEntity<>(employee, HttpStatus.CREATED);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateEmployee(@PathVariable Long id, @RequestBody Employee newEmployee) {

        Employee updatedEmployee = employeeFacade.updateEmployee(id, newEmployee);
        LOG.debug("Updated employee {}.", updatedEmployee);
        return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeEmployee(@PathVariable("id") Long id) {

        Employee employee = employeeFacade.removeEmployee(id);
        LOG.debug("Removed employee {}.", employee);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }



    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/find_by_username/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEmployeeByUserName(@PathVariable("username") String username) {

        Employee employee = employeeFacade.getEmployeeByUserName(username);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }



    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/find_by_role_name/{roleName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findEmployeesByRoleName(@PathVariable("roleName") String roleName) {

        List<Employee> employees = employeeFacade.findEmployeesByRoleName(roleName);
        if (employees.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(employees, HttpStatus.OK);
        }
    }



    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/role", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addRoleToEmployee(@PathVariable("id") Long id,
                                               @Valid @RequestBody SetRoleRequest setRoleRequest) {

        Employee employee = employeeFacade.addRoleToEmployee(id, setRoleRequest);
        LOG.debug("Role {} set to employee {}.", setRoleRequest.getRoleName(), employee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }
}