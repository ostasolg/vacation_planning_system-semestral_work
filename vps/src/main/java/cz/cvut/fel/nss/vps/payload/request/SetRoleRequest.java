package cz.cvut.fel.nss.vps.payload.request;

import lombok.Data;
import javax.validation.constraints.NotNull;


@Data
public class SetRoleRequest {

    @NotNull
    private String roleName;
}
