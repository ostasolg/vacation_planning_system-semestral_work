package cz.cvut.fel.nss.vps;


import cz.cvut.fel.nss.vps.patterns.factory.VpsFactory;
import cz.cvut.fel.nss.vps.model.*;
import cz.cvut.fel.nss.vps.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class DBInit implements CommandLineRunner {

    @Autowired
    private VpsFactory factory;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private HolidayRequestRepository holidayRequestRepository;
    @Autowired
    private StateHolidayRepository stateHolidayRepository;
    @Autowired
    private CacheManager cacheManager;


    @Override
    public void run(String... args) {

        // clear cache by name
        for(String name : cacheManager.getCacheNames()) {
            cacheManager.getCache(name).clear();
        }

        // clear database
        this.employeeRepository.deleteAll();
        this.roleRepository.deleteAll();
        this.teamRepository.deleteAll();
        this.holidayRequestRepository.deleteAll();
        this.stateHolidayRepository.deleteAll();


        String[] roleNames = {"Programmer","Tester","Developer","Assistant","Analytic"};
        Role adminr = factory.makeRole("Project Manager");

        List<Role> roles = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            roles.add(factory.makeRole(roleNames[(i)]));
        }


        String[] firstNames = {
                " Wilfred", " Candra", "Phil", "  Chae", " Werner", " Letisha", "Halley", "  Niesha", " Morris",
                "Chiquita", " Loren", " Wonda", "Joy", " Osvaldo", "Hoa", "Tiana", "Adelaide", "Garret", "Andrea",
                "Grant", "Hunter", "Sid", "Kayleigh", "Olga", "Seema", "Glayds", "Fay", "Dolly", "Darren", "Lisbeth"
        };
        String[] lastNames = {
                "Cantrell", "Cummings", "Esparza", "Key", "Payne", "Stuart", "Briggs", "Carlson", "Blair", "Hull",
                "Larson", "Wilkerson", "Poole", "Leon", "Schultz", "Cardenas", "Andrade", "Wilson", "Trujillo",
                "Ellis", "Reeves", "Porter", "Farmer", "Little", "Lewis", "Ramirez", "Ferguson", "Santos", "Snyder",
                "Bentley",
        };

        for (int i = 0; i < 11; i++) {

            if (i == 0) {
                factory.makeUser("Oli", "Ost", "user", "heslo",
                        "oliregre@oli.com", roles.get(0), 30);
            }
            else {
                factory.makeUser(firstNames[i], lastNames[i], "user" + (i), "heslo" + (i),
                        "oli@oli.com" + 5 * i, roles.get(i % 5), 25 + i);
            }
        }

        for (int i = 0; i < 9; i++) {

            if (i == 0) {
                factory.makeAdmin("Admin", "Adminovic", "admin", "password",
                        "admin@adminovic.com", adminr, 30);
            } else {
                factory.makeAdmin(firstNames[i], lastNames[i], "admin" + (i), "password" + (i),
                        "admin@adminovic.com" + 5 * i, adminr ,25 + i);
            }
        }


        List<Employee> allEmps = employeeRepository.findAll();
        List<Employee> users = allEmps.stream().filter(employee -> employee.getAuthRole().equals(AuthRole.USER))
                .collect(Collectors.toList());
        List<Employee> admins =  allEmps.stream().filter(employee -> employee.getAuthRole().equals(AuthRole.ADMIN))
                .collect(Collectors.toList());

        factory.makeHRPending(users.get(0), LocalDate.of(2021, 6, 5),
                LocalDate.of(2021, 6, 7));
        factory.makeHRPending(users.get(1), LocalDate.of(2021, 6, 29),
                LocalDate.of(2021, 7, 1));
        factory.makeHRPending(users.get(2), LocalDate.of(2021, 7, 1),
                LocalDate.of(2021, 7, 5));
        factory.makeHRPending(users.get(3), LocalDate.of(2021, 6, 10),
                LocalDate.of(2021, 6, 15));
        factory.makeHRPending(users.get(4), LocalDate.of(2021, 6, 14),
                LocalDate.of(2021, 6, 18));


        factory.makeTeam("Team" + 1, users.subList(0,2)).addEmployee(admins.get(0));
        factory.makeTeam("Team" + 2, users.subList(2,4)).addEmployee(admins.get(1));
        factory.makeTeam("Team" + 3, users.subList(4,6)).addEmployee(admins.get(2));
        factory.makeTeam("Team" + 4, users.subList(6,8)).addEmployee(admins.get(3));
        factory.makeTeam("Team" + 5, users.subList(8,10)).addEmployee(admins.get(4));

        factory.makeStateHoliday("Christmas Eve", LocalDate.of(2021, 12, 24));
        factory.makeStateHoliday("Children's day", LocalDate.of(2021, 6, 1));
        factory.makeStateHoliday("Labour day", LocalDate.of(2021, 5, 1));
        factory.makeStateHoliday("Liberation day", LocalDate.of(2021, 5, 8));
        factory.makeStateHoliday("Saints Cyril and Methodius Day", LocalDate.of(
                2021, 7, 7));
    }
}