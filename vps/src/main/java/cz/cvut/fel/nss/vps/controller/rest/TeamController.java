package cz.cvut.fel.nss.vps.controller.rest;


import cz.cvut.fel.nss.vps.patterns.facade.TeamFacade;
import cz.cvut.fel.nss.vps.model.Team;
import cz.cvut.fel.nss.vps.payload.request.SetTeamRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;



@RestController
@RequestMapping("/api/teams")
public class TeamController {


    private static final Logger LOG = LoggerFactory.getLogger(TeamController.class);

    @Autowired
    private TeamFacade teamFacade;


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllTeams() {

        List<Team> teams = teamFacade.getAllTeams();
        if (teams.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(teams, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/find_by_name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeamByName(@PathVariable("name") String name) {

        Team team = teamFacade.getTeamByName(name);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeamById(@PathVariable("id") Long id) {

        Team team = teamFacade.getTeamById(id);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createTeam(@Valid @RequestBody Team team) {

        team = teamFacade.createTeam(team);
        LOG.debug("Team {} was successfully created.", team);
        return new ResponseEntity<>(team, HttpStatus.CREATED);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateTeam(@PathVariable("id") Long id, @RequestBody Team newTeam) {

        Team updatedTeam = teamFacade.updateTeam(id, newTeam);
        LOG.debug("Updated team {}.", updatedTeam);
        return new ResponseEntity<>(updatedTeam, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeTeam(@PathVariable("id") Long id) {

        Team team = teamFacade.removeTeam(id);
        LOG.debug("Removed team {}.", team);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }



    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/employees", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addEmployeeToTeam(@PathVariable("id") Long id,
                                               @Valid @RequestBody SetTeamRequest setTeamRequest) {

        Team team = teamFacade.addEmployeeToTeam(id, setTeamRequest);
        LOG.debug("Employee with ID {} was added to team {}.", setTeamRequest.getEmpId(), team);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}/employees/{empId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removeEmployeeFromTeam(@PathVariable("id") Long id, @PathVariable("empId") Long empId) {

        Team team = teamFacade.removeEmployeeFromTeam(id, empId);
        LOG.debug("Employee with ID {} was removed from team {}.", empId, team);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }
}