package cz.cvut.fel.nss.vps.controller.rest;


import cz.cvut.fel.nss.vps.patterns.facade.RoleFacade;
import cz.cvut.fel.nss.vps.model.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;



@RestController
@RequestMapping("/api/roles")
public class RoleController {

    private static final Logger LOG = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    RoleFacade roleFacade;



    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllRoles() {

        List<Role> roles = roleFacade.getAllRoles();
        if (roles.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(roles, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getRoleById(@PathVariable("id") Long id) {

        Role role = roleFacade.getRoleById(id);
        return new ResponseEntity<>(role, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createRole(@Valid @RequestBody Role role) {

        role = roleFacade.createRole(role);
        LOG.debug("Role {} was successfully created.", role);
        return new ResponseEntity<>(role, HttpStatus.CREATED);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateRole(@PathVariable("id") Long id, @RequestBody Role newRole) {

        Role updatedRole = roleFacade.updateRole(id,newRole);
        LOG.debug("Updated role {}.", updatedRole);
        return new ResponseEntity<>(updatedRole, HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> removeRole(@PathVariable("id") Long id) {

        Role role = roleFacade.removeRole(id);
        LOG.debug("Removed role {}.", role);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}