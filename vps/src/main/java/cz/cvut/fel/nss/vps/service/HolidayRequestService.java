package cz.cvut.fel.nss.vps.service;


import cz.cvut.fel.nss.vps.exceptions.EmployeeAccessException;
import cz.cvut.fel.nss.vps.exceptions.NotFoundException;
import cz.cvut.fel.nss.vps.exceptions.ValidationException;
import cz.cvut.fel.nss.vps.model.*;
import cz.cvut.fel.nss.vps.patterns.observer.HolidayRequestObserver;
import cz.cvut.fel.nss.vps.repository.EmployeeRepository;
import cz.cvut.fel.nss.vps.repository.HolidayRequestRepository;
import cz.cvut.fel.nss.vps.repository.StateHolidayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;



@Service
public class HolidayRequestService {


    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private HolidayRequestRepository holidayRequestRepository;
    @Autowired
    private StateHolidayRepository stateHolidayRepository;
    @Autowired
    private HolidayRequestObserver holidayRequestObserver;



    @Cacheable(value = "holidayRequests")
    @Transactional
    public List<HolidayRequest> getAllHolidayRequests() {
        return holidayRequestRepository.findAll();
    }


    /**
     * Creates a new holiday request.
     */
    @CacheEvict(value = {"holidayRequests", "teams", "employees", "roles"}, allEntries = true)
    @Transactional
    public HolidayRequest create(@NotNull HolidayRequest holidayRequest) throws ValidationException {

        Employee employee = employeeRepository.getOne(Utils.getCurrent().getId());
        holidayRequest.setEmployee(employee);
        validateHolidayRequestFromEmployee(holidayRequest, employee);

        employee.addHolidayRequest(holidayRequest);
        holidayRequest.setState(HolidayRequestState.PENDING);
        employeeRepository.save(employee);
        return holidayRequestRepository.save(holidayRequest);
    }


    /**
     * Removes the existing holiday request.
     */
    @CacheEvict(value = {"holidayRequests", "teams", "employees", "roles"}, allEntries = true)
    @Transactional
    public void remove(@NotNull HolidayRequest holidayRequest) throws ValidationException {

        Objects.requireNonNull(holidayRequest.getEmployee());
        Employee employee = employeeRepository.getOne(holidayRequest.getEmployee().getId());

        if (holidayRequest.getState().equals(HolidayRequestState.APPROVED) &&
                !isHolidayRequestActual(holidayRequest)) {
            throw new ValidationException(
                    "The holiday request cannot be deleted because it has already been approved and " +
                            "is not actual anymore.");
        }
        else if (holidayRequest.getState().equals(HolidayRequestState.APPROVED)) {
            List<LocalDate> stateHolidayDates = stateHolidayRepository.findAllStateHolidayDates();
            Integer numberOfDays = holidayRequest.getNumberOfBusinessDays(
                    Optional.ofNullable(stateHolidayDates)).intValue();
            employee.addNumberOfHolidays(numberOfDays);
        }
        employee.removeHolidayRequest(holidayRequest);
        employeeRepository.save(employee);
        holidayRequest.setEmployee(null);
        holidayRequestRepository.delete(holidayRequest);
    }


    /**
     * Edits the dates of existing holiday request.
     */
    public HolidayRequest editHolidayRequest(@NotNull HolidayRequest oldHolidayRequest,
                                             @NotNull HolidayRequest newHolidayRequest) throws ValidationException {

        validateEditingHolidayRequest(oldHolidayRequest, newHolidayRequest);
        remove(oldHolidayRequest);
        try {
            return create(newHolidayRequest);
        } catch (ValidationException e) {
            oldHolidayRequest.setId(null);
            return create(oldHolidayRequest);
        }
    }


    /**
     * Validates whether the holiday request can be edited.
     */
    public void validateEditingHolidayRequest(@NotNull HolidayRequest oldHolidayRequest,
                                              @NotNull HolidayRequest newHolidayRequest) throws ValidationException {

        Objects.requireNonNull(oldHolidayRequest.getEmployee());
        Objects.requireNonNull(newHolidayRequest.getDateFrom());
        Objects.requireNonNull(newHolidayRequest.getDateTo());

        if (!isHolidayRequestActual(newHolidayRequest)) {
            throw new ValidationException(
                    "The holiday request cannot be edited because the selected new dates are not actual.");
        }
        if (oldHolidayRequest.getState().equals(HolidayRequestState.APPROVED) ||
                oldHolidayRequest.getState().equals(HolidayRequestState.REJECTED)) {
            throw new ValidationException(
                    "The holiday request cannot be edited because it has already been approved or rejected.");
        }
    }



    /**
     * Sets the status of the holiday request to APPROVED.
     */
    @CacheEvict(value = {"holidayRequests", "teams", "employees", "roles"}, allEntries = true)
    @Transactional
    public HolidayRequest approveHolidayRequest(@NotNull HolidayRequest holidayRequest) throws ValidationException {

        if (holidayRequest.getEmployee() == null) {
            throw new ValidationException(
                    "The holiday request cannot be approved because none of employees was assigned to it.");
        }
        Employee employee = employeeRepository.getOne(holidayRequest.getEmployee().getId());
        List<LocalDate> stateHolidayDates = stateHolidayRepository.findAllStateHolidayDates();
        Integer numberOfDays = holidayRequest.getNumberOfBusinessDays(
                Optional.ofNullable(stateHolidayDates)).intValue();

        if (holidayRequest.getState().equals(HolidayRequestState.APPROVED)) {
            return holidayRequest;
        }
        if (!employee.doesEmployeeHaveEnoughHolidayDays(numberOfDays)) {
            throw new ValidationException(
                    "The holiday request cannot be approved due to the lack of free holiday days of the employee. " +
                            "The holiday request should be deleted.");
        }
        if (!isHolidayRequestActual(holidayRequest)) {
            throw new ValidationException(
                    "The holiday request cannot be approved because its dates are not actual anymore." +
                            " The holiday request should be deleted.");
        }
        employee.subtractNumberOfHolidays(numberOfDays);
        holidayRequest.setState(HolidayRequestState.APPROVED);
        employeeRepository.save(employee);
        return holidayRequestRepository.save(holidayRequest);
    }


    /**
     * Sets the status of the holiday request to REJECTED.
     */
    @CacheEvict(value = {"holidayRequests", "teams", "employees", "roles"}, allEntries = true)
    @Transactional
    public HolidayRequest rejectHolidayRequest(@NotNull HolidayRequest holidayRequest) throws ValidationException {

        if (holidayRequest.getEmployee() == null) {
            throw new ValidationException(
                    "The holiday request cannot be rejected because none of employees was assigned to it.");
        }
        if (holidayRequest.getState().equals(HolidayRequestState.APPROVED) &&
                !isHolidayRequestActual(holidayRequest)) {
            throw new ValidationException(
                    "The holiday request cannot be rejected because it has already been approved and is not " +
                            "actual anymore.");
        }
        if (holidayRequest.getState().equals(HolidayRequestState.APPROVED)) {
            Employee employee = holidayRequest.getEmployee();
            List<LocalDate> stateHolidayDates = stateHolidayRepository.findAllStateHolidayDates();
            Integer numberOfDays = holidayRequest.getNumberOfBusinessDays(
                    Optional.ofNullable(stateHolidayDates)).intValue();
            employee.addNumberOfHolidays(numberOfDays);
            employeeRepository.save(employee);
        }
        holidayRequest.setState(HolidayRequestState.REJECTED);
        return holidayRequestRepository.save(holidayRequest);
    }


    /**
     * Validates the format of the dates of the holiday request.
     */
    public boolean areHolidayRequestDatesCorrect(@NotNull LocalDate from, @NotNull LocalDate to) {
        return from.isEqual(to) || from.isBefore(to);
    }


    /**
     * Validates whether the dates of the holiday request are actual.
     */
    public boolean isHolidayRequestActual(@NotNull HolidayRequest holidayRequest) {
        return (LocalDate.now().isEqual(holidayRequest.getDateFrom()) ||
                LocalDate.now().isBefore(holidayRequest.getDateFrom()));
    }


    /**
     * Validates the holiday request.
     */
    @Transactional
    public void validateHolidayRequestFromEmployee(@NotNull HolidayRequest holidayRequest,
                                                    @NotNull Employee employee) throws ValidationException {

        Objects.requireNonNull(holidayRequest.getDateFrom());
        Objects.requireNonNull(holidayRequest.getDateTo());

        List<LocalDate> stateHolidayDates = stateHolidayRepository.findAllStateHolidayDates();

        if (!areHolidayRequestDatesCorrect(holidayRequest.getDateFrom(), holidayRequest.getDateTo())) {
            throw new ValidationException("The employee cannot add or edit this holiday request, because the" +
                    " selected dates are not correct. The date when holiday starts should be" +
                    " before the day when it ends.");
        }

        Integer numberOfDays = holidayRequest.getNumberOfBusinessDays(
                Optional.ofNullable(stateHolidayDates)).intValue();

        if (!employee.doesEmployeeHaveEnoughHolidayDays(numberOfDays)) {
            throw new ValidationException("The employee cannot add or edit this holiday request due to the lack of" +
                    " free holiday days of the employee.");
        }

        if (!employee.isHolidayRequestWithoutOverlaps(holidayRequest)) {
            throw new ValidationException("The employee cannot add or edit this holiday request due to the overlaps" +
                    " with other holiday requests of this employee.");
        }

        if (!isHolidayRequestActual(holidayRequest)) {
            throw new ValidationException("The employee cannot add or edit this holiday request because the selected" +
                    " dates are not actual.");
        }
    }


    /**
     * Validates whether the employee has access to the holiday request.
     */
    @Transactional
    public void validateWhetherEmpCanGetHR(@NotNull Long holidayRequestId, @NotNull String errorMessage)
            throws EmployeeAccessException {

        Employee employee = employeeRepository.getOne(Utils.getCurrent().getId());
        boolean validationValue = employee.getHolidayRequests().stream().
                anyMatch(holidayRequest -> holidayRequest.getId().equals(holidayRequestId));

        if (validationValue || Utils.getCurrentAuthRole().equals(AuthRole.ADMIN)) {
            return;
        }
        throw new EmployeeAccessException("Employee of auth_role USER has no access to " + errorMessage + ".");
    }


    /**
     * Validates whether the employee can edit or remove the holiday request.
     */
    @Transactional
    public void validateWhetherEmpCanEditOrRemoveHR(@NotNull Long holidayRequestId, @NotNull String errorMessage)
            throws EmployeeAccessException {


        Employee employee = employeeRepository.getOne(Utils.getCurrent().getId());
        boolean validationValue = employee.getHolidayRequests().stream().
                anyMatch(holidayRequest -> holidayRequest.getId().equals(holidayRequestId));
        if (validationValue) {
            return;
        }
        throw new EmployeeAccessException("The employee does not have this holiday request and cannot " + errorMessage
                + " it.");
    }


    /**
     * Validates the state of holiday request.
     */
    public void validateHolidayRequestState(@NotNull String holidayRequestStateName) {

        try {
            HolidayRequestState.valueOf(holidayRequestStateName);
        } catch (Exception e) {
            throw NotFoundException.create("HolidayRequestState", holidayRequestStateName);
        }
    }


    /**
     * Loads the data of the holiday request.
     */
    @Transactional
    public HolidayRequest loadDataOfHolidayRequest(@NotNull Long id) throws NotFoundException {

        Optional<HolidayRequest> holidayRequestData = holidayRequestRepository.findById(id);
        if (holidayRequestData.isPresent()) {
            HolidayRequest holidayRequest = holidayRequestData.get();
            if (holidayRequest.getObservers().size() == 0) {
                holidayRequestObserver.setEntity(holidayRequest);
            }
            return holidayRequest;
        }
        throw NotFoundException.create("HolidayRequest", id);
    }


    /**
     * Gets all HRs of the team if the employee is ADMIN or only APPROVED HRs if the employee is USER.
     */
    @Transactional
    public List<HolidayRequest> findAllHolidayRequestsByTeamId(@NotNull Team team) {

        switch (Utils.getCurrentAuthRole()) {
            case ADMIN -> {
                return holidayRequestRepository.findByTeam(team);
            }
            case USER -> {
                return holidayRequestRepository.findByStateAndTeam(HolidayRequestState.APPROVED, team);
            }
            default -> {
                throw new ValidationException("Incorrect authRole.");
            }
        }
    }
}