# pull official base image
FROM maven:3.8.1-openjdk-16-slim

# set maintainer
MAINTAINER Olga Ostashchuk <olga.ostashchuk@gmail.com>

# set time zone to Europe/Prague
ENV TZ=Europe/Prague

# change current working directory
WORKDIR /usr/src/app

# copy project source files
COPY . .

# build project
RUN mvn clean package

# run project
CMD java -jar ./target/vps-0.0.1-SNAPSHOT.jar